<?php session_start(); ?><!DOCTYPE html>
<?php

if (@$_GET["destruir"]) {
    unset($_SESSION);
    session_destroy();
}

if (!$_SESSION["nomes"]) {
    $_SESSION["nomes"] = array();
}

if ($_POST["nome"]) {
    $_SESSION["nomes"][] = $_POST["nome"];
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exemplo imput array</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body style="margin: 0 30px;">
    <h1>Exemplos da aula</h1>

    <form action="index.php" method="post">
        <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text"
            class="form-control" name="nome" id="nome" aria-describedby="helpId" placeholder="Digite um nome...">
        <small id="helpId" class="form-text text-muted">Digite o nome de uma pessoa.</small>
        </div>

        <input name="enviar" id="enviar" class="btn btn-primary" type="submit" value="Enviar">
    </form>

    <div style="margin: 5px 0">
        <a name="" id="" class="btn btn-danger" href="?destruir=1" role="button">Sair da sessão</a>
    </div>

    <h2>Lista de nomes</h2>
    <ul>
        <?php for ($i=0; $i < count($_SESSION["nomes"]); $i++) { ?>
        <li><?php echo $_SESSION["nomes"][$i]; ?></li>
        <?php } ?>
    </ul>

</body>
</html>